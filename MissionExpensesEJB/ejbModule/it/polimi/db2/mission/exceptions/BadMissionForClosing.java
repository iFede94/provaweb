package it.polimi.db2.mission.exceptions;

public class BadMissionForClosing extends Exception {
	private static final long serialVersionUID = 1L;

	public BadMissionForClosing(String message) {
		super(message);
	}
}
