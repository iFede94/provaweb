package it.polimi.db2.mission.exceptions;

public class BadMissionReporter extends Exception {
	private static final long serialVersionUID = 1L;

	public BadMissionReporter(String message) {
		super(message);
	}
}
